import { combineReducers } from 'redux';
import authentification from './authentification';

const rootReducer = combineReducers({
  authentification: authentification
});

export default rootReducer;
