const INITIAL_STATE = {
  authenticated: false,
  error: ''
};
  
export default function (state = {}, action) {
  switch(action.type) {
    case 'AUTH_USER': 
      return { ...state, authenticated: true, error: '' };
    default: 
      return INITIAL_STATE;
  }
  return state;
}
