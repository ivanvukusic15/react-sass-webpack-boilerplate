'use strict';

import React from 'react';
import { Link } from 'react-router';

export const Header = () =>{
  return (
    <header id = "header">
      HEADER
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/page">Page</Link></li>
          <li><Link to="/contact">Contact</Link></li>
        </ul>
      </nav>
    </header>
  );
};
