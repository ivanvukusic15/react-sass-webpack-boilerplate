import React, { Component } from 'react';
import { connect } from 'react-redux';

class NotFound extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className="404-page">
          404 page
      </section>
    );
  }

}

export default connect(null)(NotFound);