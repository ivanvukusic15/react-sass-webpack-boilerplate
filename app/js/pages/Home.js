import React, { Component } from 'react';
import { connect } from 'react-redux';

class Home extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section className="home-page">
          Home page
      </section>
    );
  }

}

export default connect(null)(Home);