import React from 'react';
import ReactDOM from 'react-dom';

// if ( process.env.NODE_ENV !== 'production' ) {
//   window.React = React;
// }
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import reduxThunk from 'redux-thunk';
import reducers from './reducers';

import App from './App';
import Home from './pages/Home';
import Page from './pages/Page';
import Contact from './pages/Contact';
import NotFound from './pages/404page';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

ReactDOM.render(
  <Provider store = { store }>
    <Router history = { browserHistory }>
      <Route path = "/" component = { App }>
        <IndexRoute component = { Home } />
        <Route path = "/page" component = { Page } />
        <Route path = "/contact" component = { Contact } />
        <Route path = "/*" component = { NotFound } />
      </Route>
    </Router>
  </Provider>
, document.getElementById('app'));
